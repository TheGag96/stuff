import std.stdio, std.algorithm, std.conv, std.exception, std.uni, std.functional;

void main() {
  funcTable = [
    "set"      : toDelegate(&funcSet),
    "+"        : toDelegate(&funcPlus),
    "-"        : toDelegate(&funcMinus),
    "*"        : toDelegate(&funcMultiply),
    "/"        : toDelegate(&funcDivide),
    "%"        : toDelegate(&funcModulo),
    "printvar" : toDelegate(&funcPrintvar),
    "if"       : toDelegate(&funcIf),
    "while"    : toDelegate(&funcWhile),
    "=="       : toDelegate(&funcEqual),
    "!="       : toDelegate(&funcNotEqual),
    ">"        : toDelegate(&funcGreater),
    "<"        : toDelegate(&funcLess),
    ">="       : toDelegate(&funcGreaterEqual),
    "<="       : toDelegate(&funcLessEqual),
    //"for"      : toDelegate(&funcFor),
    "func"     : toDelegate(&funcFunc)
  ];

  write(">");

  foreach (line; stdin.byLine) {
    try {
      writeln(parseAndRun(line.idup));
    }
    catch (Error e) {
      writeln("Oops, looks like you made a syntax error or something.");
    }

    write(">");
  }
}

/*
  S    -> ( op arg ) S | lambda
  op   -> ...
  arg  -> type arg | var arg | lambda
  type -> int | float | bool | string
  var  -> ident
*/

double delegate(string[])[string] funcTable;

double[string] varTable;

double parseAndRun(string s) {
  double result;

  int level = 0;
  Stack!(string[]) commands;

  while (s.length) {
    string token = s.nextToken;

    if (token == "(") {
      level++;
      commands.push([]);
    }
    else if (token == ")") {
      auto funcToRun = funcTable[commands.top[0]];
      auto args = (commands.top.length == 1) ? [] : commands.top[1..$];
      
      result = funcToRun(args);

      commands.pop;
      if (commands.empty) commands.push([]); 
      commands.top ~= result.to!string;

      level--;
    }
    else {
      enforce(level > 0);      
      commands.top ~= token;
    }
  }

  enforce(level == 0);

  return result;
} 

string nextToken(ref string s) {
  string result;

  if (!s.length) return s;

  while (s.length > 0 && [' ', '\t', '\r', '\n'].canFind(s[0])) {
    s = s[1..$];
  }

  //delegate literals
  if (s.length && s[0] == '{') {
    size_t counter = 0;
    int level = 0;

    do {
      if (s[counter] == '{') level++;
      else if (s[counter] == '}') level--;
      counter++;
    } while (level > 0);

    result = s[0..counter];
    s = s[counter..$];
    return result;
  }

  size_t endPoint = 0;

  while (endPoint < s.length && ![' ', '\t', '\r', '\n', '(', ')'].canFind(s[endPoint])) {
    endPoint++;
  }

  if (s[0] == '(' || s[0] == ')') {
    result = s[0..1];
    s = s[1..$];
  }
  else {
    result = s[0..endPoint];
    s = s[endPoint..$];
  }

  return result;
}

struct Stack(T) {
  import std.array: Appender, appender;
  Appender!(T[]) _app;
  @property ref inout(T) top() inout { return _app.data[$ - 1]; };
  @property bool empty() const { return _app.data.length == 0; }
  void pop() { _app.shrinkTo(_app.data.length - 1); }
  void push(T t) { _app.put(t); }
}

////
// begin functions
////

double funcSet(string[] args) {
  enforce(args.length == 2 && isIdentifier(args[0]));

  double result = getVarOrVal(args[1]);
  varTable[args[0]] = result;

  return result;
}

mixin template BinaryFunc(string name, string op) {
  mixin("double func" ~ name ~ "(string[] args) {
    enforce(args.length == 2);

    double a = getVarOrVal(args[0]), b = getVarOrVal(args[1]);

    static if (op.length >= 3) {
      mixin(\"return \" ~ op ~ \"(a, b);\");
    }
    else {
      mixin(\"return a \" ~ op ~ \" b;\");
    }
  }");
}

mixin BinaryFunc!("Plus", "+");
mixin BinaryFunc!("Minus", "-");
mixin BinaryFunc!("Multiply", "*");
mixin BinaryFunc!("Divide", "/");
mixin BinaryFunc!("Modulo", "%");
mixin BinaryFunc!("Equal", "==");
mixin BinaryFunc!("NotEqual", "!=");
mixin BinaryFunc!("Greater", ">");
mixin BinaryFunc!("Less", "<");
mixin BinaryFunc!("GreaterEqual", ">=");
mixin BinaryFunc!("LessEqual", "<=");

private double getVarOrVal(string arg) {
  try {
    return arg.to!double;
  }
  catch (Exception e) {
    if (double* ptr = arg in varTable) {
      return *ptr;
    }
    else {
      return double.nan;
    }
  } 
}

double funcPrintvar(string[] args) {
  enforce(args.length == 0);
  varTable.byKeyValue.each!(x => writeln(x.key, ": ", x.value));
  return double.nan;
}

double funcIf(string[] args) {
  enforce(args.length == 2 || args.length == 3);
  enforce(isFunctionLiteral(args[0]) && isFunctionLiteral(args[1]));

  //(if {(== x 3)} {(set x 5)})

  string cond = args[0][1..$-1];
  double result;

  if (parseAndRun(cond) != 0) {
    result = parseAndRun(args[1][1..$-1]);
  }
  else if (args.length == 3) {
    enforce(isFunctionLiteral(args[2]));
    result = parseAndRun(args[2][1..$-1]);
  }

  return result;
}

double funcWhile(string[] args) {
  enforce(args.length == 2);
  enforce(isFunctionLiteral(args[0]) && isFunctionLiteral(args[1]));

  double result;
  string cond = args[0][1..$-1], loopBody = args[1][1..$-1];


  while (parseAndRun(cond) != 0) {
    result = parseAndRun(loopBody);
  }

  return result;
}

double funcFunc(string[] args) {
  enforce(args.length == 3 && args[0] !in funcTable && isFunctionLiteral(args[2]));

  string funcBody = args[2][1..$-1];
  int numVars = cast(int)getVarOrVal(args[1]);

  enforce(numVars >= 0);

  double delegate(string[]) newFunc = (string[] funcArgs) {
    enforce(funcArgs.length == numVars);

    foreach (i, x; funcArgs) {
      varTable["$" ~ (i+1).to!string] = getVarOrVal(x);
    }

    double result = parseAndRun(funcBody);

    foreach (i, x; funcArgs) {
      varTable.remove("$" ~ (i+1).to!string);
    }

    return result;
  };

  funcTable[args[0]] = newFunc;

  return double.nan;
}

bool isFunctionLiteral(string arg) {
  return (arg[0] == '{' && arg[$-1] == '}');
}

bool isIdentifier(string arg) {
  if (!arg[0].isAlpha) return false;

  foreach (c; arg) {
    if (!c.isAlpha && !c.isNumber && c != '_') return false;
  }

  return true;
}