module equation;

import std.stdio, std.algorithm, std.conv, std.typecons, std.random, std.math, std.meta;

void main() {
  variables["x"] = 3;
  variables["y"] = 8;

  foreach (i; 0..10) {
    Function func = Function.randomlyGenerate;
    writeln(func.toString, " = ", func.evaluate);
  }
}

//alias VarValue = Tuple!(string, "name", string, "value");

public double[string] variables;

interface Node {
  double evaluate();
  string toString();
}

class Function : Node {
  Node start;

  this() { }
  this(Node start) { this.start = start; }

  double evaluate() { return start.evaluate; }
  override string toString() { return start.toString; } 

  static Function randomlyGenerate() {
    auto vars = variables.keys;
    static auto operators = [OPS];

    Node recursive() {
      switch (uniform!"[]"(0, 2)) {
        case 0:
          return new Value(uniform!"[]"(-100.0, 100.0));
        break;

        case 1:
          return new Variable(vars[uniform(0, vars.length)]);
        break;

        case 2:
          switch (operators[uniform(0, operators.length)]) {
            foreach (op; OPS) {
              case op:
                return new Operator!op(recursive(), recursive());
              break;
            }

            default: break;
          }
        break;

        default: break;
      }

      assert(0);
    }

    Function result = new Function(recursive());
    return result;
  }
}

class Value : Node {
  double val;

  this(double val) { this.val = val; }

  double evaluate() { return this.val; }
  override string toString() { return this.val.to!string; }
}

class Variable : Node {
  string var;

  this(string var) { this.var = var; }
  double evaluate() { return variables[var]; }
  override string toString() { return var; }
}

alias OPS = AliasSeq!("+", "-", "*", "/", "%", "^^");

class Operator(string op) : Node {
  Node left, right;

  this(Node left, Node right) {
    this.left = left;
    this.right = right;
  }

  double evaluate() { 
    static if (op.length >= 3) {
      return mixin(op ~ "(this.left.evaluate, this.right.evaluate)");
    }
    else {
      return mixin("this.left.evaluate " ~ op ~ " this.right.evaluate");
    }
  }
  
  override string toString() { 
    static if (op.length >= 3) {
      return op ~ "(" ~ this.left.toString ~ ", " ~ this.right.toString ~ ")";
    }
    else {
      return "(" ~ this.left.toString ~ " " ~ op ~ " " ~ this.right.toString ~ ")";
    }
  }
}