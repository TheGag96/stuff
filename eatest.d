import ea, std.algorithm, std.stdio, std.random, std.math;

alias Dub = Gene!(double, -1000000, 1000000);
alias FuncVal = Organism!(Dub, "x", Dub, "y");

double runFunc(FuncVal val) {
  return val.x*val.x + val.y*val.y;
  //return 25 - val.x*val.x*val.x*val.x - 3 * val.y*val.y + val.y - val.x;
  //return sin(0.1*val.x) + 5*cos(0.8*val.y);
}

double avg(R)(R stuff) {
  double count = 0;
  double sum = 0;

  foreach (x; stuff) {
    sum += x;
    count++;
  }

  return sum / count;
}

void main() {
  FuncVal[10000] population;
  population[] = FuncVal.spawn;
  FuncVal.setFitnessFunction(&runFunc);

  foreach (a; 0..1000) {
    population[].each!((ref f) => f.testFitness);
    population[].sort!"a > b";

    writeln(population[].map!(f => f.x.value).avg, " ", population[].map!(f => f.y.value).avg);

    foreach (b; 0..1000) {
      population[$-b-1] = population[2*b].breed(population[2*b+1]);

      if (uniform!"[]"(0, 5) == 0) {
        population[$-b-1].x.randomize;
      }

      if (uniform!"[]"(0, 5) == 0) {
        population[$-b-1].y.randomize;
      }
    }
  }

  population[].sort!"a > b";
  writeln("\n", population[0].x.value, " ", population[0].y.value, " ", population[0].fitness);
}