module ea;

import std.stdio, std.algorithm, std.array, std.random, std.typecons, std.variant, std.mathspecial, std.meta, std.traits, std.conv, std.math;

struct Gene(T, T low, T high, string interval="[]") {
  T value;
  alias value this;

  this(T value) {
    this.value = limitToBounds(value);
  }

  void randomize() {
    this.value = uniform!interval(low, high);
  }

  /**
   * Combines genes using a normal distribution from the average of both values
   */
  auto combine(Gene!(T, low, high, interval) other) {
    auto newValue = cast(T) ( (this.value+other.value)/2.0 + normalDistributionInverse(uniform!"()"(0.0, 1.0)) );
    newValue = limitToBounds(newValue);

    Gene!(T, low, high, interval) result = newValue;

    return result;
  }
  
  /**
   * Combines genes with weighting based on two given fitness values
   */
  auto combine(Gene!(T, low, high, interval) other, double fitnessThis, double fitnessOther) {
    double ratio = fitnessThis/fitnessOther;
    double closeness;
    double zeroPoint;

    if (this > other) {
      closeness = ratio/(ratio+1);
      zeroPoint = other + (this-other) * closeness;
    }
    else {
      closeness = 1 - ratio/(ratio+1);
      zeroPoint = this + (other-this) * closeness;
    }

    auto newValue = cast(T) ( zeroPoint + normalDistributionInverse(uniform!"()"(0.0, 1.0)) );
    newValue = limitToBounds(newValue);

    Gene!(T, low, high, interval) result = newValue;

    return result;
  }

  private T limitToBounds(T thing) {
    if (thing >= high) {
      static if (interval[1] == ')') {
        static if (isIntegral!T) {
          return high - 1;
        }
        else {
          return high.nextDown();
        }
      }
      else {
        return high;
      }
    }
    else if (thing <= low) {
      static if (interval[0] == '(') {
        static if (isIntegral!T) {
          return low + 1;
        }
        else {
          return low.nextUp();
        }
      }
      else {
        return low;
      }
    }
    else return thing; 
  }
}

struct Gene(T) if (is(T : bool) || is(T : string)) {
  private bool value;
  alias value this;

  this(bool value) {
    this.value = value;
  }

  void randomize() {
    this.value = cast(T) uniform!"[]"(0, 1);
  }

  auto combine(Gene!T other) {
    if (this.value == other.value) return Gene!T(this.value);
    else return Gene!T();
  }
}

/**
 * Base Organism template. Instatiate it like this:
 * alias SomeOrg = Organism!(Gene!(args1), "name1", Gene!(args2), "name2", ...);
 */
struct Organism(G...) {
  mixin(ctGenerateGeneMembers());

  double fitness;
  static double function(Organism!G) fitnessFunc;

  /**
   * Globally sets fitness function for species
   */
  static void setFitnessFunction(double function(Organism!G) fitnessFunc) {
    this.fitnessFunc = fitnessFunc;
  }

  /**
   * Comparison operators for sorting
   */
  int opCmp(Organism!G other) {
    if (this.fitness < other.fitness) return -1;
    else if (this.fitness > other.fitness) return 1;
    else return 0;
  }

  bool opEquals(Organism!G other) {
    return this.fitness == other.fitness;
  }

  /**
   * Runs the fitness function set to be run for the species.
   */
  void testFitness() {
    if (fitnessFunc is null) {
      throw new Exception("Cannot test with a null fitness function");
    }

    this.fitness = fitnessFunc(this);
  }

  /**
   * Breeds two genes, randomly taking advantage of 3 different approaches:
   * - Point-split geneset and recombine
   * - Randomly choose which parents' gene to copy
   * - Average with normally distributed offset each parents' gene
   */
  Organism!G breed(Organism!G other) {
    switch (uniform!"[]"(0, 2)) {
      //split geneset and recombine
      case 0:
        auto thisGenes = this.genes;
        auto otherGenes = other.genes;
        Variant[G.length/2] childGenes;

        auto splitPoint = uniform(0, thisGenes.length);

        foreach (a; 0..splitPoint) {
          childGenes[a] = thisGenes[a];
        }

        foreach (a; splitPoint..thisGenes.length) {
          childGenes[a] = otherGenes[a];
        }

        return Organism!G(childGenes);

        break;

      //randomly pick genes from both
      case 1:
        auto thisGenes = this.genes;
        auto otherGenes = other.genes;
        Variant[G.length/2] childGenes;

        foreach (a; 0..thisGenes.length) {
          if (uniform!"[]"(0, 1)) {
            childGenes[a] = thisGenes[a];
          }
          else {
            childGenes[a] = otherGenes[a];
          }
        }

        return Organism!G(childGenes);

        break;

      //combine/average genes from both
      case 2:
        Organism!G child;
        mixin(ctCombineGenes!G);

        return child;

        break;

      default: return Organism!G();
    }
  }  

  /**
   * Function whose result is mixed in and calls all the other code-building functions
   */
  private static string ctGenerateGeneMembers() {
    auto app = appender!string;

    ctBuildDeclarationString!G(app);

    app.put("\n@property Variant[");
    app.put((G.length/2).to!string);
    app.put("] genes() { return [");
    ctBuildVariantArrayGetter!G(app);
    app.put("]; }");

    app.put("\nprivate this(Variant[G.length/2] geneSet) {\n");
    ctBuildVariantConstructor!(0, G)(app);
    app.put("}\n");

    app.put("\npublic static Organism!G spawn() {\nOrganism!G result;\n");
    ctBuildInitFunction!(G)(app);
    app.put("return result;\n}\n");

    return app.data;
  }

  /**
   * Builds the code that declares all genes
   */
  private static void ctBuildDeclarationString(T...)(Appender!string app) {
    static if (T.length > 0) {
      app.put(T[0].stringof);
      app.put(" ");
      app.put(T[1]);
      app.put(";\n");

      ctBuildDeclarationString!(T[2..$])(app);
    }
  }

  /**
   * Builds the body of the function that returns the gene set as a static Variant array
   */
  private static void ctBuildVariantArrayGetter(T...)(Appender!string app) {
    static if (T.length > 0) {
      app.put("Variant(");
      app.put(T[1]);
      app.put(")");

      static if (T.length > 2) {
        app.put(", ");
      }

      ctBuildVariantArrayGetter!(T[2..$])(app);
    }
  }

  /**
   * Builds the body of the constructor that takes a gene set as an argument
   */
  private static void ctBuildVariantConstructor(size_t index, T...)(Appender!string app) {
    static if (T.length > 0) {
      app.put("this.");
      app.put(T[1]);
      app.put(" = geneSet[");
      app.put(index.to!string);
      app.put("].get!(");
      app.put(T[0].stringof);
      app.put(");\n");

      ctBuildVariantConstructor!(index+1, T[2..$])(app);
    }
  }

  /**
   * Builds the spawn() function used to properly instantiate the organism
   */
  private static void ctBuildInitFunction(T...)(Appender!string app) {
    static if (T.length > 0) {
      app.put("result.");
      app.put(T[1]);
      app.put(".randomize();\n");

      ctBuildInitFunction!(T[2..$])(app);
    }
  }

  /**
   * Builds the code that calls combine() on each gene of the organism
   */
  private static string ctCombineGenes(T...)(Appender!string app = appender!string) {
    static if (T.length == 0) return app.data;
    else {
      app.put("child.");
      app.put(T[1]);
      app.put(" = this.");
      app.put(T[1]);
      app.put(".combine(other.");
      app.put(T[1]);
      app.put(");\n");

      return ctCombineGenes!(T[2..$])(app);
    }
  }
}

/*
//test
alias MaxLives = Gene!(int, 1, 9);
alias CanFly = Gene!(bool);
alias Weight = Gene!(double, 0.1, 20, "()");
alias Cat = Organism!(MaxLives, "maxLives", CanFly, "canFly", Weight, "weight");

void main() {
  Cat mrWhiskers = Cat.spawn, fluffy = Cat.spawn;
  Cat garfield = mrWhiskers.breed(fluffy);

  writeln(mrWhiskers.weight, " ", fluffy.weight, " ", garfield.weight);
}*/