module automata;

import std.stdio, std.algorithm, std.array, std.string, std.range, std.conv, std.traits;

template Automata(int[] acceptStates, T...) {
  bool Automata(inout(char)[] s) {
    mixin(buildString!(acceptStates, T));
  }
}

string buildString(int[] acceptStates, T...)() {
  auto app = appender!string;

  app.put("import std.algorithm : canFind;

           int state = 1;

           foreach (c; s) {
             switch (state) {\n");

  chunk3Apply!(appendCase, T)(app);

  app.put("    default: return false; break;
             }
           }");

  app.put("\n\nif (");
  app.put(acceptStates.stringof);
  app.put(".canFind(state)) {
             return true;
           }
           else {
             return false;
           }");

  return app.data;
}

void chunk3Apply(alias func, R...)(Appender!string app) {
  static if (!R.length) return;
  else {
    func!(R[0..3])(app);
    chunk3Apply!(func, R[3..$])(app);
  }
}

void appendCase(S...)(Appender!string app) {
  static assert (isIntegral!(typeof(S[0])) &&
                 isSomeString!(ElementType!(typeof(S[1]))) &&
                 isIntegral!(ElementType!(typeof(S[2]))) &&
                 S[1].length == S[2].length);  

  app.put("case ");
  app.put(S[0].to!string);
  app.put(":\n");

  foreach (i; 0..S[1].length) {
    if (i != 0) {
      app.put("else ");
    }
    app.put(buildIfStatement(S[1][i]));
    app.put(" { state = ");
    app.put(S[2][i].to!string);
    app.put("; }\n");
  }

  app.put("else { return false; }\n");
  app.put("break;\n");
}

string buildIfStatement(string chars) {
  auto app = appender!string;
  app.put("if (");

  bool firstCond = true;

  for (int k = 0; k < chars.length; k++) {
    if (firstCond) {
      firstCond = false;
    }
    else {
      app.put(" || ");
    }

    char c = chars[k];

    if (c == '\\') {
      if (k < chars.length-1) {
        char escapeChar = chars[k+1];
        app.put("c == '");
        
        if (escapeChar == 'n' || c == 'r') {
          app.put('\\');
        }

        app.put(escapeChar);
        app.put('\'');

        k++;
      }
      else throw new Exception("Why is there a backslash here?");
    }
    else if (k < chars.length-1 && chars[k+1] == '-') {
      if (k >= chars.length-2) throw new Exception("There was a hyphen but we're at the end of the string");

      char other = chars[k+2];

      if (c > other) throw new Exception("Left sise of hyphen must be less than the right");

      app.put("(c >= '");
      app.put(c);
      app.put("' && c <= '");
      app.put(other);
      app.put("')");

      k += 2;
    }
    else if (c == '-') {
      throw new Exception("Why is there a hyphen here?");
    }
    else {
      app.put("c == '");
      app.put(c);
      app.put('\'');
    }
  }

  app.put(")");

  return app.data;
}

unittest {
  alias isIdentifier = Automata!(
    [2],
    1, [`a-zA-Z_`], [2],
    2, [`a-zA-Z_0-9`], [2]
  );

  assert (isIdentifier("asdSsdf")    == true);   
  assert (isIdentifier("3FfFf")      == false); 
  assert (isIdentifier("_")          == true); 
  assert (isIdentifier("Asdf_fd.fd") == false);
}